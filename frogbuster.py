# First attempt at a Directory Buster
# Written by Blunt Moak Frog 2022

import datetime
import requests
import sys
import time
from termcolor import cprint

class enum:
    def __init__(self, url, wordlist, ext):
        self.url = url
        self.wordlist = open(wordlist, "r+")
        self.ext = ext

    def logger(self, sub, log):
        cprint("[!] - Subdomain found at:" + sub, "green")
        time.sleep(0.5)
        cprint("[|] - Adding to log...", "yellow")
        logfile = open(log, "a")
        logfile.write(sub + "\n")
        time.sleep(0.5)
    
    def attack(self, log):
        for i in range(2000):
            subdomain = self.wordlist.readline(10).strip()
            subURL = self.url + subdomain + self.ext
            response = requests.get(subURL)
            if (response.status_code == 200):
                self.logger(subURL, log)
            else:
                cprint("[?] - Subdomain " + subURL + " not found", "red")

def main():
    time.sleep(2)
    cprint('''
                    ______                 ____             __           
                   / ____/________  ____ _/ __ )__  _______/ /____  _____
                  / /_  / ___/ __ \/ __ `/ __  / / / / ___/ __/ _ \/ ___/
                 / __/ / /  / /_/ / /_/ / /_/ / /_/ (__  ) /_/  __/ /    
                /_/   /_/   \____/\__, /_____/\__,_/____/\__/\___/_/     
                                 /____/                                  
    ''', "green")
    time.sleep(1)
    cprint('''
                                    °***°°°°°°°°.                                             
                                   oOOOOOOooOOOoOO°                                           
                                  .OOOOOOOOOOOOOOO.                                           
                        ..°*°..    °oo########Oo                                              
                   °*°°°°**oOOoo*°.   #########*     .°**°°°°*°oOo°                           
                  *@O**°.°°°o#ooOOOOooOOOOOoooo*°°°*oOo*Oo*°°*°O##O°                          
                  °##o*****o#OoOOOOOOOoooooooooOOoOOOOo*oOooo*oOo*o*                          
                   .ooooooOOOoOOOOOOooooooooooooOoOOOOOoooOOOoooooo.                          
                     oOOOOOOOOOOOoooooOooooooooooooOOOOOOOoooooooo.                           
                     OOOOOOOOOOOOOoooooooo**oooooooooOO##@####OOO                             
                    *oooooooooooooo***************ooooooOOOOOOOOo                             
                    o#OOOOOOOOOOOOOOOOOOOOOOOOOOOooOOOOOOOOoooo***                            
                     *ooOOOOooooooooooooooooOOoooOoOOOO#@O°........°**.                       
                        ...°*oo************oooooooooooooOOo**°°.°°°OOOo                       
                             °oOOo*******ooo*****oooooOO**°°.     .°°°                        
                           .oo.*##Oo******o******ooOO#o°*°                                    
                         °O##@o°°o##Oo***oooooOOOOOOo*°o@##O°                                 
                      .*O@#####o.°*oO#OOOOO##OOOo**°.o########o°                              
                     o######@@#@*.°°°°°**o******°°°°O###@@####@#O°                            
                    O@#####@o° O#°..°*ooOOOo°°°°°°°O#o  .o@#######O.                          
                   o@#####@°   o#O*oOOOOOOOOOO*°°*O###°   °@@@@####O                          
                   O##@###@.  oO#@#####ooOoO#@@###@###O    °#@@@O###°                         
                   O###@#@@#. °####@##Oooooo#@@@@######*    O@@@#O###°                        
                   O@#@@@@@@@O#######OooooooO#@@##O#@###...*#@@@@#####*                       
                   °OOoO###@@@@#OO@###oooooooO@@#@#O#@@@@@#@@@@@#oooooo                       
                    .*oO##@#@@##O#####OoooooOO@@##@#O#@@#OOO#@@@OoOoo*                        
    ''', "green")
    time.sleep(1)
    cprint("[ Directory Buster by Blunt Moak Frog ]", "green")
    time.sleep(1)
    try:
        cprint("[|] - Attempting to enumerate URL...")
        strike = enum(sys.argv[1], sys.argv[2], sys.argv[3])
        date = datetime.datetime.now()
        newLog = "frogbuster-check-{}-{}-{}-{}.{}.{}.txt".format(date.year, date.month, date.day, date.hour, date.minute, date.second)
        time.sleep(1)
    except:
        cprint("[?] - Syntax error: Please check provided args and ensure you've provided the correct spellings and/or arg amount", "red")
        sys.exit(0)
    strike.attack(newLog)
    cprint("[!] - Process done, check directory for logs", "green")

if __name__ == "__main__":
    main()
