### FROGBUSTER - Where Yo Dirs At????

#### Features:
- Busts that directory, baby
- idk it's a test for me at this point

#### Usage:
Download the Git repository and use as following:
```bash
$ git clone https://gitlab.com/frogsquad/frogbuster.git
$ cd frogbuster
$ python3 frogbuster.py <url> <wordlist> <ext>
```

Receive busted directories. Remember kids, hacking is illegal, use this tool only for ethical purposes.
